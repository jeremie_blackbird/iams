**iams** (Install A Magento Server)

This **bash shell** script is destined to people who:

* Have a fresh installed Debian
* Want to run quickly and easily a Magento server, ready for the production

In a few words, iams is going to install apache, nginx, postfix and every necessary vhosts and configure users.

Still in development, you can fork it or making pull request !

Have fun !

Jérémie