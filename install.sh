#!/bin/bash
#
# Script d'installation d'un serveur web complet pour Magento
# Auteur : Jérémie Bouchet <jeremie@bird.eu>
# Version : 1.1.0
#

# ##########################################################################

source configuration.sh

if [[ $EUID -ne 0 ]]; then
    echo ""
    echo -e "${CRED}/!\ ERREUR: This script has to be executed by root.${CEND}" 1>&2
    echo -e "${CRED}Careful, on Ubuntu, we have to specify interpreter. Eg: sudo bash install.sh ${CEND}" 1>&2 
    echo ""
    exit 1
fi

source functions.sh

# ##########################################################################

clear

echo ""
echo -e "${CYELLOW}  IAMS (Install A Magento Server) - Hosting Magento 2 ${CEND}"
echo -e "${CYELLOW}  More information on https://bitbucket.org/jeremie_blackbird/iams/overview ${CEND}"
echo ""
echo -e "${CCYAN}
██████╗ ██╗██████╗ ██████╗    ███████╗██╗   ██╗
██╔══██╗██║██╔══██╗██╔══██╗   ██╔════╝██║   ██║
██████╔╝██║██████╔╝██║  ██║   █████╗  ██║   ██║
██╔══██╗██║██╔══██╗██║  ██║   ██╔══╝  ██║   ██║
██████╔╝██║██║  ██║██████╔╝██╗███████╗╚██████╔╝
╚═════╝ ╚═╝╚═╝  ╚═╝╚═════╝ ╚═╝╚══════╝ ╚═════╝ 
${CEND}"
echo ""

confirm "Are you sure to continue? [y|N]" || exit 1


exit 1;

echo -ne 'Vérification des pré-requis... \r'

if [ ! -d ${logDir} ]; then
	mkdir -p ${logDir}
fi

# Vérification des exécutables
verifInstall aptitude
verifInstall wget
verifInstall vim

#TODO Add an option to verifInstall to exit if the package not exists (and re-code verifInstall to not exit) (maybe "important" or "required")
verifInstall apache2 notexist
verifInstall nginx notexist
verifInstall percona notexist
verifInstall phpmyadmin notexist
verifInstall logwatch notexist

apt-get update >/dev/null 2>>${logFile}
#apt-get dist-upgrade >/dev/null 2>>${logFile}

echo -ne "Vérification des pré-requis... ${CRED}OK${CEND} \r"
echo -ne '\n'
echo -ne '\n'

### CONFORT DE BASE ###
# Activation de la coloration syntaxique de vi / vim
# Raccourcis
# Synchronisation de l'heure

installAccessories

echo -ne '\n'


# Installation de logwatch

# Script iptables


# ##########################################################################

# ##########################################################################


#installSecurite
#installFail2ban
#installPostfix


# ##########################################################################

exit

DOMAIN=$(hostname -d 2> /dev/null)   # domain.tld
HOSTNAME=$(hostname -s 2> /dev/null) # hostname
FQDN=$(hostname -f 2> /dev/null)     # hostname.domain.tld

# Récupération de l'adresse IP WAN
WANIP=$(dig +short myip.opendns.com @resolver1.opendns.com)


exit 0
