#!/bin/bash

CSI="\033["
CEND="${CSI}0m"
CRED="${CSI}1;31m"
CGREEN="${CSI}1;32m"
CYELLOW="${CSI}1;33m"
CBLUE="${CSI}1;34m"
CPURPLE="${CSI}1;35m"
CCYAN="${CSI}1;36m"
CBROWN="${CSI}0;33m"

logDir="/var/log/install_server"
logFile=${logDir}"/"`date "+%Y-%m-%d-%H:%M:%S"`".log"

sshConfigFile="/etc/ssh/sshd_config"

fail2banConfigFile="/etc/fail2ban/jail.conf"
fail2banJailConfigFile="/etc/fail2ban/jail.local"
fail2banJailConfigFileSource="$(pwd)/bin-conf/jail.local"

vimConfigFile="/etc/vim/vimrc"

rootBashrc="/root/.bashrc"