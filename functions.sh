#!/bin/bash

smallLoader() {
    echo ""
    echo ""
    echo -ne '[ + + +             ] 3s \r'
    sleep 1
    echo -ne '[ + + + + + +       ] 2s \r'
    sleep 1
    echo -ne '[ + + + + + + + + + ] 1s \r'
    sleep 1
    echo -ne '[ + + + + + + + + + ] Appuyez sur [ENTRÉE] pour continuer... \r'
    echo -ne '\n'

    read
}

requiredBin() {
    echo -e "${CRED}/!\ ERREUR: Le programme '$1' est requis pour cette installation.${CEND}"
}

alreadyHereBin() {
    echo -e "${CRED}/!\ ERREUR: '$1' est déjà installé sur le serveur.${CEND}" 1>&2
}

displayErrorAndQuit() {
    echo -e "\n ${CRED}/!\ OUPS ! Une erreur est survenue pendant l'installation de $1.${CEND}" 1>&2
    exit 1
}

# 20150108
# Get 2 parameters :
# $1: name of package to check
# $2: state to check ("exist" / null OR "notexist")
verifInstall() {
	dpkg -l "$1" 2> /dev/null | grep '^ii' &> /dev/null
	test=$?
	if [ "$2" = "notexist" ]; then
		if [ $test -eq 0 ]; then
		    alreadyHereBin $1
		    exit 1
		fi
	else
		if [ $test -eq 1 ]; then
		    requiredBin $1
		    exit 1
		fi
	fi
}

confirm () {
    # call with a prompt string or use a default
    read -r -p "${1:-Are you sure? [y/N]} " response
    case $response in
        [yY][eE][sS]|[yY]) 
            true
            ;;
        *)
            false
            ;;
    esac
}

replaceConfig () {
    search=$1
    replace=$2
    filename=$3
    
    if grep -q "$search" $filename; then
        sed -i -e "s#$search.*#$replace#" $filename
    elif grep -q "$replace" $filename; then
        echo -e "${CYELLOW}WARNING: Config '${replace}' already exists in ${filename}${CEND}"
    else
        echo $replace >> $filename
    fi
}

setConfig () {
    add=$1
    filename=$2
    
    if grep -q "$add" $filename; then
        echo -e "${CYELLOW}WARNING: Config '${add}' already exists in ${filename}${CEND}"
    else
        echo $add >> $filename
    fi
}

##################################################################
##################################################################

installAccessories() {
	echo -e "${CBLUE}Configuration of VIM for comfort (colors, hour sync, aliases)${CEND}"
	
	replaceConfig "\"syntax on" "syntax on" ${vimConfigFile}
	replaceConfig "\"set background=dark" "set background=dark" ${vimConfigFile}
	
	# Raccourcis de root
    setConfig "export LS_OPTIONS='--color=auto'" ${rootBashrc}
    setConfig 'eval \"`dircolors`\"' ${rootBashrc}
    setConfig "alias ls='ls $LS_OPTIONS'" ${rootBashrc}
    setConfig "alias ll='ls $LS_OPTIONS -l'" ${rootBashrc}
    setConfig "alias l='ls $LS_OPTIONS -lA'" ${rootBashrc}

	# Synchro heure
	apt-get install -y ntp ntpdate
}


installSecurite() {
	# 1. SECURITE DE BASE – OBLIGATOIRE
	echo -e "${CBLUE}1) Global & basic security configuration${CEND}"
	
	# 2. Changer le port SSH (pour le 6767)
	read -p "- Please enter the new SSH port on which to listen: (actual:`more ${sshConfigFile} | grep Port | sed s/Port/\/`) [Default : 6767] " SSHPORT
	if [ "${SSHPORT}" = "" ]; then
	    SSHPORT="6767"
	fi
	replaceConfig "Port" "Port ${SSHPORT}" ${sshConfigFile}
	
	# 3. Root can't connect
	echo -ne "- Prevent root login:\r"
	replaceConfig "PermitRootLogin" "PermitRootLogin no" ${sshConfigFile}
	echo -ne "- Prevent root login: ${CRED}OK${CEND} \r"
	echo -ne '\n'
}


installFail2ban() {
	verifInstall fail2ban notexist
	echo -ne "- Installation et configuration de fail2ban \r"
	
	apt-get install -y fail2ban >${logFile} 2>&1
	if [ $? -ne 0 ]; then
	    displayErrorAndQuit fail2ban
	fi
	
	read -p "- Adresse(s) IP des administrateurs séparées par des espaces [Par défaut : 88.180.49.224] " IPADMIN
    if [ "${IPADMIN}" = "" ]; then
        IPADMIN="88.180.49.224"
    fi
    
    read -p "- Adresse email de monitoring du serveur [Par défaut : monitoring@bird.eu] " EMAILADMIN
    if [ "${EMAILADMIN}" = "" ]; then
        EMAILADMIN="monitoring@bird.eu"
    fi
	
	fail2banLook="ignoreip = 127.0.0.1\/8"
	fail2banReplace="${fail2banLook} ${IPADMIN}"
	sed -i "s/${fail2banLook}.*/${fail2banReplace}/" ${fail2banConfigFile}
	
	fail2banLook="bantime"
	fail2banReplace="${fail2banLook} = 8640000\nfindtime = 86400"
	sed -i "s/${fail2banLook}.*/${fail2banReplace}/" ${fail2banConfigFile}
	
	fail2banLook="destemail"
	fail2banReplace="${fail2banLook} = ${EMAILADMIN}"
	sed -i "s/${fail2banLook}.*/${fail2banReplace}/" ${fail2banConfigFile}
	
	cat "${fail2banJailConfigFileSource}" > "${fail2banJailConfigFile}"
	
	fail2banLook="port = ssh"
	fail2banReplace="${fail2banLook},${SSHPORT}"
	sed -i "s/${fail2banLook}.*/${fail2banReplace}/" ${fail2banJailConfigFile}
	
	fail2ban-client reload >${logFile} 2>&1
	
	echo -ne "- Installation et configuration de fail2ban ${CRED}OK${CEND}\r"
	fail2ban-client status
}

installPostfix() {
	verifInstall postfix notexist
	# Installation de postfix
	
	apt-get install -y postfix 2>${logFile}
	if [ $? -ne 0 ]; then
	    displayErrorAndQuit postfix
	fi
}